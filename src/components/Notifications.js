import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import { Box, Typography } from "@mui/material";

import { Notification, ExtendTime } from "./";

const GUEST_ENTERS_ROOM = 1;
const GUEST_PRESS_PRIVACY_BUTTON = 2;
const GUEST_OPENS_FROM_INSIDE = 3;
const TURN_OFF_AFTER_GUEST_OPENS_FROM_INSIDE = 4;
const GUEST_PRESS_TURN_OFF_NOW = 5;
const ABOUT_TO_EXPIRE_GUEST_EXTENDED_TIME = 6;

const STATES = {
  [GUEST_ENTERS_ROOM]: [
    {
      title: "Welcome, John Doe.",
      description:
        "To activate the AC, push the green ring button on the door lock.",
      time: "Now",
    },
  ],

  [GUEST_PRESS_PRIVACY_BUTTON]: [
    {
      title: "Hello, John Doe.",
      description: "The AC in your room is now on.",
      time: "Now",
    },
    {
      title: "Welcome, John Doe.",
      description:
        "To activate the AC, push the green ring button on the door lock.",
      time: "5 seconds ago",
    },
  ],

  [GUEST_OPENS_FROM_INSIDE]: [
    {
      title: "Hello, John Doe.",
      description: (
        <>
          <Typography gutterBottom>
            In line with our sustainability values, the AC will turn off in 60
            seconds.
          </Typography>
          <Typography>
            To keep the AC on, push the green ring button on the door lock.
          </Typography>
        </>
      ),
      time: "Now",
    },
  ],

  [TURN_OFF_AFTER_GUEST_OPENS_FROM_INSIDE]: [
    {
      title: "Hello, John Doe.",
      description: "In line with our sustainability values, the AC is now off.",
      time: "Now",
    },
    {
      title: "Hello, John Doe.",
      description: (
        <>
          <Typography gutterBottom>
            In line with our sustainability values, the AC will turn off in 60
            seconds.
          </Typography>
          <Typography>
            To keep the AC on, push the green ring button on the door lock.
          </Typography>
        </>
      ),
      time: "1 minute ago",
    },
  ],

  [GUEST_PRESS_TURN_OFF_NOW]: [
    {
      title: "Hello, John Doe.",
      description: "In line with our sustainability values, the AC is now off.",
      time: "Now",
    },
  ],

  [ABOUT_TO_EXPIRE_GUEST_EXTENDED_TIME]: [
    {
      title: "Hello, John Doe.",
      description: (
        <>
          <Typography gutterBottom>
            In line with our sustainability values, the AC will turn off in 5
            minutes.
          </Typography>
          <Typography>
            To keep the AC on, push the green ring button on the door lock.
          </Typography>
        </>
      ),
      time: "Now",
    },
    {
      title: "Hello, John Doe.",
      description: "The AC will be kept on for an additional 10 minutes.",
      time: "5 minutes ago",
    },
    {
      title: "Hello, John Doe.",
      description: (
        <>
          <Typography gutterBottom>
            In line with our sustainability values, the AC will turn off in 10
            minutes.
          </Typography>
          <Typography>
            To keep the AC on, push the green ring button on the door lock.
          </Typography>
        </>
      ),
      time: "15 minute ago",
    },
  ],
};

const Notifications = ({ state }) => {
  const notificationsWrapper = useRef(null);
  const [topShadowOpacity, setTopShadowOpacity] = useState(0);
  const [bottomShadowOpacity, setBottomShadowOpacity] = useState(0);

  useEffect(() => {
    handleNotificationsScroll();
  }, []);

  const handleNotificationsScroll = () => {
    const { scrollTop, scrollHeight, clientHeight } =
      notificationsWrapper.current;
    const bottomScrollTop = scrollHeight - clientHeight;

    setTopShadowOpacity((1 / 20) * Math.min(scrollTop, 20));
    setBottomShadowOpacity(
      (1 / 20) * (bottomScrollTop - Math.max(scrollTop, bottomScrollTop - 20))
    );
  };

  const shadowStyle = {
    position: "absolute",
    left: 0,
    right: 0,
    height: 50,
  };

  const shadowTopStyle = {
    top: 0,
    opacity: topShadowOpacity,
    background:
      "linear-gradient(to bottom, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0) 100%)",
  };

  const shadowBottomStyle = {
    bottom: -1,
    opacity: bottomShadowOpacity,
    background:
      "linear-gradient(to top, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0) 100%)",
  };

  // from 1 to 6
  const currentScene = STATES[state];

  return (
    <>
      <Box position="relative">
        <Box
          ref={notificationsWrapper}
          onScroll={handleNotificationsScroll}
          sx={{
            maxHeight: "67vh",
            overflowX: "scroll",
          }}
        >
          {currentScene.map((message, index) => (
            <Notification key={index} texts={message} />
          ))}
        </Box>

        <Box
          sx={{
            ...shadowStyle,
            ...shadowTopStyle,
          }}
        />

        <Box
          sx={{
            ...shadowStyle,
            ...shadowBottomStyle,
          }}
        />
      </Box>

      {state === TURN_OFF_AFTER_GUEST_OPENS_FROM_INSIDE && <ExtendTime />}
    </>
  );
};

Notifications.propTypes = {
  state: PropTypes.any,
};

export default Notifications;
