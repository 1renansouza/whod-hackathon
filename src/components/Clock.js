import React from "react";
import moment from "moment";

import { Box, Typography } from "@mui/material";

const Clock = () => {
  const date = moment();

  return (
    <Box mt={6} mb={3} textAlign="center" color="common.white">
      <Typography variant="h2" component="p">
        {date.format("LT")}
      </Typography>
      <Typography variant="subtitle1" component="p">
        {date.format("LL")}
      </Typography>
    </Box>
  );
};

export default Clock;
