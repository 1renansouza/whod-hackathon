import React from "react";
import PropTypes from "prop-types";

import { Grid, Box, Typography } from "@mui/material";

const Notification = ({ texts }) => {
  const { title, description, time } = texts;

  return (
    <Box
      py={1.5}
      px={2}
      m={1.5}
      sx={{
        borderRadius: "15px",
        backgroundColor: "#b6cfccb8",
        overflow: "hidden",
        position: "relative",
      }}
    >
      <Box
        sx={{
          position: "absolute",
          backgroundColor: "#b6cfcc75",
          left: 0,
          right: 0,
          top: 0,
          height: "52px",
        }}
      />
      <Box
        sx={{
          position: "relative",
        }}
      >
        <Grid container spacing={1} alignItems="center">
          <Grid item>
            <Box
              width={30}
              height={30}
              sx={{
                borderRadius: 1,
                backgroundImage: "url(/img/icon.webp)",
                backgroundSize: 30,
              }}
            />
          </Grid>
          <Grid item xs>
            <Typography>SALTO KS</Typography>
          </Grid>
          <Grid item>
            <Typography variant="body2">{time}</Typography>
          </Grid>
        </Grid>
      </Box>

      <Box
        mt={2.3}
        sx={{
          fontWeight: "bold",
        }}
        component={Typography}
      >
        {title}
      </Box>
      <Typography component="div">{description}</Typography>
    </Box>
  );
};

Notification.propTypes = {
  texts: PropTypes.shape({
    title: PropTypes.any.isRequired,
    description: PropTypes.any.isRequired,
    time: PropTypes.any.isRequired,
  }).isRequired,
};

export default Notification;
