import React from "react";

import { ButtonGroup, Button, Box, Typography } from "@mui/material";
import { Close } from "@mui/icons-material";

import { Notification } from ".";

const ExtendTime = () => (
  <>
    <Box
      sx={{
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        filter: "blur(10px)",
        background: "#1a664cbd",
      }}
    />
    <Box
      sx={{
        position: "absolute",
        top: 0,
      }}
    >
      <Notification
        texts={{
          title: "Hello, John Doe.",
          description: (
            <>
              <Typography gutterBottom>
                In line with our sustainability values, the AC will turn off in
                10 minutes.
              </Typography>
              <Typography>
                To keep the AC on, push the green ring button on the door lock.
              </Typography>
            </>
          ),
          time: <Close />,
        }}
      />

      <Box
        component={ButtonGroup}
        orientation="vertical"
        px={2}
        sx={{
          width: "100%",
        }}
      >
        <Box
          component={Button}
          sx={{
            backgroundColor: "#b6cfccb8 !important",
            height: "50px",
            borderRadius: "15px",
            textTransform: "none",
            color: "black",
            borderColor: "#b6cfccb8 !important",
          }}
        >
          <Typography>Extend 10 minutes</Typography>
        </Box>
        <Box
          component={Button}
          sx={{
            backgroundColor: "#b6cfccb8 !important",
            height: "50px",
            borderRadius: "15px",
            textTransform: "none",
            color: "black",
            borderColor: "#b6cfccb8 !important",
          }}
        >
          <Typography>Turn off now</Typography>
        </Box>
      </Box>
    </Box>
  </>
);

export default ExtendTime;
