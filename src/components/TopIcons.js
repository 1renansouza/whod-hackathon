import React from "react";

import { Grid, Box, Typography } from "@mui/material";
import { NetworkCell, NetworkWifi, Battery80 } from "@mui/icons-material";

const TopIcons = () => (
  <Box p={1} color="common.white">
    <Grid container spacing={1}>
      <Grid item>
        <NetworkCell />
      </Grid>
      <Grid item xs>
        <NetworkWifi />
      </Grid>

      <Grid item>
        <Typography component="span">80%</Typography>
      </Grid>
      <Grid item>
        <Box
          component={Battery80}
          sx={{
            transform: "rotate(90deg)",
          }}
        />
      </Grid>
    </Grid>
  </Box>
);

export default TopIcons;
