import React, { useEffect, useState } from "react";
import axios from "axios";

import { TopIcons, Clock, Notifications } from "./components";

function App() {
  const [currentState, setCurrentState] = useState(1);

  useEffect(() => {
    startPolling();
  }, []);

  const startPolling = () => {
    setInterval(() => {
      axios
        .get("https://jsonplaceholder.typicode.com/posts/1")
        .then((result) =>
          setCurrentState(Math.floor(Math.random() * (6 - 1) + 1))
        );
    }, 1500);
  };

  return (
    <div
      style={{
        backgroundImage: "url(/img/background.png)",
        backgroundPosition: "right",
        height: "100vh",
      }}
    >
      <TopIcons />
      <Clock />
      <Notifications state={currentState} />
    </div>
  );
}

export default App;
